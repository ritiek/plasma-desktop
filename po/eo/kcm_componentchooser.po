# translation of kcm_componentchooser.pot to esperanto
# Copyright (C) 2005-2023 Free Software Foundation, Inc.
# Matthias Peick <matthias@peick.de>, 2005.
# Axel Rousseau <axel584@axel584.org>, 2007.
# Pierre-Marie Pédrot <pedrotpmx@wanadoo.fr>, 2007.
# Michael Moroni <michael.moroni@mailoo.org>, 2012.
# Oliver Kellogg <okellogg@users.sourceforge.net>, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: kcmcomponentchooser\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-10-13 01:43+0000\n"
"PO-Revision-Date: 2023-10-13 07:27+0100\n"
"Last-Translator: Oliver Kellogg <okellogg@users.sourceforge.net>\n"
"Language-Team: Esperanto <kde-i18n-eo@kde.org>\n"
"Language: eo\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.4\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: applicationmodel.cpp:67
#, kde-format
msgid "Other…"
msgstr "Alia…"

#: components/componentchooserarchivemanager.cpp:15
#, kde-format
msgid "Select default archive manager"
msgstr "Elekti defaŭltan arkivmastrumilon"

#: components/componentchooserbrowser.cpp:13
#, kde-format
msgid "Select default browser"
msgstr "Elekti defaŭltan retumilon"

#: components/componentchooseremail.cpp:20
#, kde-format
msgid "Select default e-mail client"
msgstr "Meti defaŭltan retpoŝtilon"

#: components/componentchooserfilemanager.cpp:15
#, kde-format
msgid "Select default file manager"
msgstr "Elekti la defaŭltan dosiermastrumilon"

#: components/componentchoosergeo.cpp:13
#, kde-format
msgid "Select default map"
msgstr "Elekti la defaŭltan mapon"

#: components/componentchooserimageviewer.cpp:15
#, kde-format
msgid "Select default image viewer"
msgstr "Elekti la defaŭltan bildmontrilon"

#: components/componentchoosermusicplayer.cpp:15
#, kde-format
msgid "Select default music player"
msgstr "Elekti la defaŭltan muzikreproduktilon"

#: components/componentchooserpdfviewer.cpp:11
#, kde-format
msgid "Select default PDF viewer"
msgstr "Elekti defaŭltan PDF-montrilon"

#: components/componentchoosertel.cpp:17
#, kde-format
msgid "Select default dialer application"
msgstr "Elekti la defaŭltan numerum-aplikaĵon"

#: components/componentchooserterminal.cpp:25
#, kde-format
msgid "Select default terminal emulator"
msgstr "Elekti la defaŭltan terminal-imitilon"

#: components/componentchoosertexteditor.cpp:15
#, kde-format
msgid "Select default text editor"
msgstr "Elektu la defaŭltan tekstredaktilon"

#: components/componentchooservideoplayer.cpp:15
#, kde-format
msgid "Select default video player"
msgstr "Elekti la defaŭltan videoreproduktilon"

#: ui/ComponentOverlay.qml:52
#, kde-format
msgid "Details"
msgstr "Detaloj"

#: ui/ComponentOverlay.qml:82
#, kde-format
msgid ""
"This application does not advertise support for the following file types:"
msgstr "Tiu aplikaĵo ne reklamas subtenon pot la sekvaj dosiertipoj:"

#: ui/ComponentOverlay.qml:96
#, kde-format
msgctxt "@action:button"
msgid "Force Open Anyway"
msgstr "Devigi malfermon ĉiukaze"

#: ui/ComponentOverlay.qml:113
#, kde-format
msgid ""
"The following file types are still associated with a different application:"
msgstr "La sekvontaj dosiertipoj estas ankoraŭ asociitaj kun malsama aplikaĵo:"

#: ui/ComponentOverlay.qml:126
#, kde-format
msgctxt "@label %1 is a MIME type and %2 is an application name"
msgid "%1 associated with %2"
msgstr "%1 asociita kun %2"

#: ui/ComponentOverlay.qml:139
#, kde-format
msgctxt "@action:button %1 is an application name"
msgid "Re-assign-all to %1"
msgstr "Re-assign-all al %1"

#: ui/ComponentOverlay.qml:150
#, kde-format
msgid "Change file type association manually"
msgstr "Ŝanĝi dosiertipan asociiĝon permane"

#: ui/main.qml:17
#, kde-format
msgid ""
"’%1’ seems to not support the following mimetypes associated with this kind "
"of application: %2"
msgstr ""
"’%1’ ŝajnas ne subteni la sekvantajn mime-tipojn rilatatajn al ĉi speco de "
"aplikaĵo: %2"

#: ui/main.qml:46
#, kde-format
msgctxt "Internet related application’s category’s name"
msgid "Internet"
msgstr "Interreto"

#: ui/main.qml:50
#, kde-format
msgid "Web browser:"
msgstr "Retumilo:"

#: ui/main.qml:72
#, kde-format
msgid "Email client:"
msgstr "Retpoŝtilo:"

#: ui/main.qml:94
#, kde-format
msgctxt "Default phone app"
msgid "Phone Numbers:"
msgstr "Telefonnumeroj:"

#: ui/main.qml:116
#, kde-format
msgctxt "Multimedia related application’s category’s name"
msgid "Multimedia"
msgstr "Multmedia"

#: ui/main.qml:121
#, kde-format
msgid "Image viewer:"
msgstr "Bildrigardilo:"

#: ui/main.qml:145
#, kde-format
msgid "Music player:"
msgstr "Muzikreproduktilo:"

#: ui/main.qml:168
#, kde-format
msgid "Video player:"
msgstr "Videoreproduktilo:"

#: ui/main.qml:190
#, kde-format
msgctxt "Documents related application’s category’s name"
msgid "Documents"
msgstr "Dokumentoj"

#: ui/main.qml:195
#, kde-format
msgid "Text editor:"
msgstr "Tekstredaktilo:"

#: ui/main.qml:217
#, kde-format
msgid "PDF viewer:"
msgstr "PDF-rigardilo:"

#: ui/main.qml:239
#, kde-format
msgctxt "Utilities related application’s category’s name"
msgid "Utilities"
msgstr "Utilaĵoj"

#: ui/main.qml:244
#, kde-format
msgid "File manager:"
msgstr "Dosiermastrumilo:"

#: ui/main.qml:266
#, kde-format
msgid "Terminal emulator:"
msgstr "Terminal-imitilo:"

#: ui/main.qml:279
#, kde-format
msgid "Archive manager:"
msgstr "Arkivmastrumilo:"

#: ui/main.qml:301
#, kde-format
msgctxt "Map related application’s category’s name"
msgid "Map:"
msgstr "Mapo:"
