# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma_toolbox_org\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-10-11 02:07+0000\n"
"PO-Revision-Date: 2023-01-09 09:58+0000\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: contents/ui/ToolBoxContent.qml:266
#, kde-format
msgid "Choose Global Theme…"
msgstr "Escolher o Tema Global…"

#: contents/ui/ToolBoxContent.qml:273
#, kde-format
msgid "Configure Display Settings…"
msgstr "Configurar o Ecrã…"

#: contents/ui/ToolBoxContent.qml:294
#, kde-format
msgctxt "@action:button"
msgid "More"
msgstr "Mais"

#: contents/ui/ToolBoxContent.qml:308
#, kde-format
msgid "Exit Edit Mode"
msgstr "Sair do Modo de Edição"
